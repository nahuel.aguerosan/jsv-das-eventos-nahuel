var vector = [10, 'Hola', true, 3.14, false, 'Mundo'];

console.log(vector);

console.log('Primer elemento:', vector[0]);
console.log('Último elemento:', vector[vector.length - 1]);

vector[2] = 'Nuevo valor';

console.log('Longitud del vector:', vector.length);

vector.push('Nuevo elemento');

var ultimoElemento = vector.pop();
console.log('Último elemento eliminado:', ultimoElemento);

vector.splice(3, 0, 'Elemento en la mitad');

var primerElemento = vector.shift();
console.log('Primer elemento eliminado:', primerElemento);

vector.unshift(primerElemento);

console.log('Vector actualizado:', vector);
