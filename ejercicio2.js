var vector = [10, 'Hola', true, 3.14, false, 'Mundo'];

for (var i = 0; i < vector.length; i++) {
  console.log(vector[i]);
}

vector.forEach(function(valor) {
  console.log(valor);
});

vector.map(function(valor) {
  console.log(valor);
});

var i = 0;
while (i < vector.length) {
  console.log(vector[i]);
  i++;
}

for (var valor of vector) {
  console.log(valor);
}
